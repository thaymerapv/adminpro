import {Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';

@Component({
  selector: 'app-incrementador',
  templateUrl: './incrementador.component.html',
  styles: []
})
export class IncrementadorComponent implements OnInit {
  @ViewChild('txtprogress') txtProgress: ElementRef;
  @Input() progress = 50;
  @Input() leyenda = 'Leyenda';

  @Output() cambioValor: EventEmitter<number> = new EventEmitter();

  constructor() {
  }

  ngOnInit(): void {
  }

  onCHanges(newValue: number) {
    // const elementHTML: any = document.getElementsByName('progress')[0];
    if (newValue >= 100) {
      this.progress = 100;
    } else if (newValue <= 0) {
      this.progress = 0;
    } else {
      this.progress = newValue;
    }
    // elementHTML.value = Number(this.progress);
    this.txtProgress.nativeElement.value = this.progress;
    this.cambioValor.emit(this.progress);
    this.txtProgress.nativeElement.focus();
  }

  changeValue(value: number) {
    if (this.progress >= 100 && value > 0) {
      this.progress = 100;
      return;
    }
    if (this.progress <= 0 && value < 0) {
      this.progress = 0;
      return;
    }
    this.progress = this.progress + value;
    this.cambioValor.emit(this.progress);
  }
}
