import {Component, OnInit} from '@angular/core';
import {SettingsService} from '../../services/settings/settings.service';

@Component({
  selector: 'app-account-settings',
  templateUrl: './account-settings.component.html',
  styles: []
})
export class AccountSettingsComponent implements OnInit {

  constructor(
    public Ajustes: SettingsService
  ) {
  }

  ngOnInit(): void {
    this.cargarcheck();
  }

  cambiarColor(tema: string, link: any) {
    this.aplicarCheck(link);
    this.Ajustes.aplicarTema(tema);
  }

  aplicarCheck(link: any) {
    const selectores: any = document.getElementsByClassName('selector');
    for (const ref of selectores) {
      ref.classList.remove('working');
    }
    link.classList.add('working');
  }

  cargarcheck() {
    const selectores: any = document.getElementsByClassName('selector');
    const tema = this.Ajustes.ajustes.tema;
    for (const ref of selectores) {
      if (ref.getAttribute('data-theme') === tema) {
        ref.classList.add('working');
        break;
      }
    }
  }
}
