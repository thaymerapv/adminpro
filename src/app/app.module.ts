import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
// tslint:disable-next-line:jsdoc-format
/** Rutas **/
import {AppRoutingModule} from './app-routing.module';
// tslint:disable-next-line:jsdoc-format
/** Modules **/
import {PagesModule} from './pages/pages.module';
// tslint:disable-next-line:jsdoc-format
/** Components **/
import {AppComponent} from './app.component';
import {LoginComponent} from './login/login.component';
import {RegisterComponent} from './login/register.component';
import {FormsModule} from '@angular/forms';
import {ServiceModule} from './services/service.module';

// import { GraficaDonaComponent } from './components/grafica-dona/grafica-dona.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    // GraficaDonaComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    PagesModule,
    FormsModule,
    ServiceModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
